#!/usr/bin/env python

import rospy
import sys
import cv2
import cv2.cv as cv
import numpy as np


from std_msgs.msg import String
from sensor_msgs.msg import Image, CameraInfo 	

from cv_bridge import CvBridge, CvBridgeError

from matplotlib import pyplot as plt

class LaserListenerNode():
    def __init__(self):
        self.node_name = 'laser_listener_node'
        rospy.init_node(self.node_name)
        rospy.on_shutdown(self.cleanup)

        """ Create a cv window """
        self.cv_window_name = self.node_name
        cv.NamedWindow(self.cv_window_name, cv.CV_WINDOW_NORMAL)
        cv.MoveWindow(self.cv_window_name, 25, 75)

        """ Create a cv_bridge object """
        self.bridge = CvBridge()

        """ Subscribe to the raw camera image topic and beacon angle """
        self.image_sub = rospy.Subscriber("/camera/rgb/image_raw", Image, self.callback, queue_size=1)
        #self.beacon_angle = rospy.Subscriber("/camera/rgb/image_raw", Image, self.callback, queue_size=1)

        rospy.loginfo("Waiting for image topics...")

    def callback(self, ros_image):
        # Use cv_bridge() to convert the ROS image to OpenCV format
        try:
            frame = self.bridge.imgmsg_to_cv(ros_image, "bgr8")
            #print 'Messsage Received'
        except CvBridgeError, e:
            print e
        
        # Convert the image to a Numpy array since most cv2 functions
        # require Numpy arrays.
        frame = np.array(frame, dtype=np.uint8)
        
        # Process the frame using the process_image() function
        display_image = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
                       
        # Display the image.
        cv2.imshow(self.node_name, display_image)

        # Process any keyboard commands
        self.keystroke = cv.WaitKey(5)
        if 32 <= self.keystroke and self.keystroke < 128:
            cc = chr(self.keystroke).lower()
            if cc == 'q':
                # The user has pressed the q key, so exit
                rospy.signal_shutdown("User hit q key to quit.")
            if cc == 'h':
                # The user has pressed the h key, so get histogram data
                self.process_image(frame)

    def process_image(self, frame):
        # Convert to HSV
        img_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        # Histograms
        # create a mask
        img = img_hsv
        mask = np.zeros(img.shape[:2], np.uint8)
        mask[:, img.shape[1]*0.3:img.shape[1]*0.6,] = 255
        masked_img = cv2.bitwise_and(img,img,mask = mask)

		# Calculate histogram with mask and without mask
		# [0] - H channel, mask, [256] - H bins, [0,256] - H range
        hist_full = cv2.calcHist([img],[0],None,[256],[0,256])
        hist_mask = cv2.calcHist([img],[0],mask,[256],[0,256])
        np.savetxt("foo.csv", hist_mask, delimiter=",")

        print hist_mask.max()
        print hist_mask.argmax()
        
        plt.subplot(221), plt.imshow(img, 'gray')
        plt.subplot(222), plt.imshow(mask,'gray')
        plt.subplot(223), plt.imshow(masked_img, 'gray')
        plt.subplot(224), plt.plot(hist_full), plt.plot(hist_mask)
        plt.xlim([0,256])

        hist = plt.show()

        return


    def cleanup(self):
        print "Shutting down laser_listener_node."
        cv2.destroyAllWindows()  

if __name__ == '__main__':
    try:
        LaserListenerNode()
        rospy.spin()
    except KeyboardInterrupt:
        self.cleanup()
        cv.DestroyAllWindows()