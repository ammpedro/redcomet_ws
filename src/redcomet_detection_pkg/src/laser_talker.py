#!/usr/bin/env python
import rospy

from std_msgs.msg import String

def laser_talker():
  pub = rospy.Publisher('laser_chatter', String, queue_size=10)
  rospy.init_node('laser_talker', anonymous=True)
  r = rospy.Rate(10)
  while not rospy.is_shutdown():
    str = "hello %s"%rospy.get_time()
    rospy.loginfo(str)
    pub.publish(str)
    r.sleep()

if __name__ == '__main__':
  try:
    laser_talker()
  except rospy.ROSInterruptException: pass