#!/usr/bin/env python
import rospy

import PyKDL
from math import pi, radians, sqrt, pow

from std_msgs.msg import String
from geometry_msgs.msg import Twist, Point, Quaternion

import tf

class DriveSquare():
  def __init__(self):
    # node name
    rospy.init_node('square_routine', anonymous=True)

    # on exit
    rospy.on_shutdown(self.shutdown)

    # movement publisher
    self.pub = rospy.Publisher('/mobile_base/commands/velocity', Twist)

    # odometry controls
    self.tf_listener = tf.TransformListener()
    self.odom_frame = '/odom' 

    # check for published tf
    try:
      self.tf_listener.waitForTransform(self.odom_frame, 'base_footprint', rospy.Time(), rospy.Duration(1.0))
      self.base_frame = '/base_footprint'
    except (tf.exception, tf.ConnectivityException, tf.LookupException):
      try:
        rospy.loginfo("Did not find /base_footprint topic, looking for /base_link")
        self.tf_listener.waitForTransform(self.odom_frame, 'base_link', rospy.Time(), rospy.Duration(1.0))
        self.base_frame = '/base_link'
      except (tf.exception, tf.ConnectivityException, tf.LookupException):
        rospy.loginfo("Cannot find transform between /odom and /base_footprint or /base_link")
        rospy.signal_shutdown("tf exception")

    # update rate
    rate = 10
    r = rospy.Rate(rate)

    # speed, distance, time (move forward)
    linear_speed = 0.2                      # meters/sec
    goal_distance = 1.0                     # meters
  
    # rotation speed
    angular_speed = 0.25                     # radians/sec

    # rotation angle 90 degrees per corner
    goal_angle = radians(90)                  # radians

    # set angle tolerance to 2.5 degrees
    angular_tolerance = radians(2)

    # set position variable
    position = Point()

    # drive
    for i in range(4):
      # init/reset move_cmd
      move_cmd = Twist()

      # setup cmd speed
      move_cmd.linear.x = linear_speed

      # get start position
      (position, rotation) = self.get_odom()

      x_start = position.x
      y_start = position.y

      distance = 0

      rospy.loginfo("Turtlebot Forward")
      # move to goal_distance
      while distance < goal_distance and not rospy.is_shutdown():
          self.pub.publish(move_cmd)
          r.sleep()

          # check distance traveled via Euclidean distance
          (position, rotation) = self.get_odom()
          distance = sqrt(pow(position.x - x_start, 2) + pow(position.y - y_start, 2))

      # stop bot
      move_cmd = Twist()
      self.pub.publish(move_cmd)
      r.sleep()

      # setup cmd to rotate 90 deg
      move_cmd.angular.z = angular_speed

      # track angle
      last_angle = rotation
      turn_angle = 0

      rospy.loginfo("Turtlebot Rotate")
      # rotate 90 deg
      while abs(turn_angle + angular_tolerance) < abs(goal_angle) and not rospy.is_shutdown():
        self.pub.publish(move_cmd)
        r.sleep()

        # check rotation performed
        # check distance traveled via Euclidean distance
        (position, rotation) = self.get_odom()
        delta_angle = self.normalize_angle(rotation - last_angle)

        # integrate
        turn_angle += delta_angle
        last_angle = rotation

      # stop bot
      rospy.loginfo(last_angle)
      move_cmd = Twist()
      self.pub.publish(move_cmd)
      r.sleep()

    self.pub.publish(Twist())

  def get_odom(self):
    try:
      (trans, rot) = self.tf_listener.lookupTransform(self.odom_frame, self.base_frame, rospy.Time(0))
    except (tf.Exception, tf.ConnectivityException, tf.LookupException):
      rospy.loginfo("tf exception")
      return

    return (Point(*trans), self.quat_to_angle(Quaternion(*rot))) 

  def shutdown(self):
    # for stopping the robot when node shutsdown
    rospy.loginfo("Goodbye Turtlebot...")
    self.pub.publish(Twist())
    rospy.sleep(1)

  def quat_to_angle(self, quat): 
    # convert quaternion to euler angle (for odom)
    rot = PyKDL.Rotation.Quaternion(quat.x, quat.y, quat.z, quat.w)
    return rot.GetRPY()[2]
        
  def normalize_angle(self, angle):
    # to c 180 and -180 degrees; 0 and 360 degrees
    res = angle
    while res > pi:
        res -= 2.0 * pi
    while res < -pi:
        res += 2.0 * pi
    return res


if __name__ == '__main__':
  try:
    DriveSquare()
  except rospy.ROSInterruptException: pass
