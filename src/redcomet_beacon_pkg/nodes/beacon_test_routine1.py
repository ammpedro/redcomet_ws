#! /usr/bin/env python

# beacon test routine1
# description: code to determine ground truth values vs encoder feedback

import roslib
import rospy

from dynamixel_msgs.msg import MotorState
from std_msgs.msg import Float64
from std_msgs.msg import String

import math

class BeaconTestNode():

     def __init__(self):
          #Initialize new node
          rospy.init_node('beacon_test_routine1_node', anonymous=True)
          
          #Initialize Motor States
          self.motor1_state = MotorState()
          self.motor2_state = MotorState()

          #dynamixel_test_namespace = rospy.get_namespace()
          dynamixel_test_namespace = 'dynamixel_controller/'
          rate = rospy.get_param('~rate', 1)
          r = rospy.Rate(rate)

		#Subscribe to motor status topic
          rospy.Subscriber('/dynamixel_controller/motor1_controller/state', MotorState, self.motor1_state)
          rospy.Subscriber('/dynamixel_controller/motor2_controller/state', MotorState, self.motor2_state)
		
          #Publish beacon status topic
          self.pub = rospy.Publisher('counter', Float64)

		#Initialize publisher for servo1/top motor
          self.servo1_frame = 'servo1_link'
          self.servo1_pub = rospy.Publisher(dynamixel_test_namespace + 'motor1_controller/command', Float64)

		#Initialize publisher for servo2/bottom motor
          self.servo2_frame = 'servo2_link'
          self.servo2_pub = rospy.Publisher(dynamixel_test_namespace + 'motor2_controller/command', Float64)

		#Initialize servo1/2 positions to -90deg
          rospy.sleep(1)
          self.initialize_servos()
          rospy.loginfo("Laser Beacon Initialized")

          #Motor Movement Loop
          self.beacon_test()
				
     def initialize_servos(self):
          # Reset Servo 1 position to -90deg
          rospy.loginfo("Resetting Servo 1")	
          self.servo1_pub.publish(-1.57);
          # Reset Servo 2 position to -90deg
          rospy.loginfo("Resetting Servo 2")
          self.servo2_pub.publish(-1.57)
          rospy.sleep(10)
          

     def beacon_test(self):
          cnt = 0
          theta_s2 = -1.57
          theta_p = 0
          for cnt in range(19):
          #Move servo2-bottom cw (0-180 @ 10degree intervals)
               self.servo2_pub.publish(theta_s2)
               theta_p =  theta_s2 + 0.1745329252
               rospy.loginfo(cnt)
               self.pub.publish(int(cnt))
               rospy.sleep(7)
               theta_s2 = theta_s2 + 0.1745329252

          cnt = 0
          for cnt in range(19):
          #Move servo2-bottom ccw (180-0 @ 10degree intervals)
               self.servo2_pub.publish(theta_s2)
               theta_p =  theta_s2 - 0.1745329252
               rospy.loginfo(cnt)
               self.pub.publish(int(cnt))
               rospy.sleep(7)
               theta_s2 = theta_s2 - 0.1745329252


if __name__ == '__main__':
     try:
          start_beacon = BeaconTestNode()
          rospy.spin()
     except rospy.ROSInterruptException: pass 


