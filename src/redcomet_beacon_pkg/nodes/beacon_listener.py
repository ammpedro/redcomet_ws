#! /usr/bin/env python

import roslib; roslib.load_manifest('redcomet')
import rospy

from dynamixel_msgs.msg import MotorState
from std_msgs.msg import Float64
from std_msgs.msg import String

def beacon_listener():
     #Initialize Motor States
     motor1_state = MotorState()
     motor2_state = MotorState()

     #Subscribe 
     rospy.Subscriber('/dynamixel_controller/motor1_controller/state', MotorState, motor1_state)
     rospy.loginfo(str(motor1_state.position))

if __name__ == '__main__':
     try:
          beacon_listener()
          rospy.spin()
     except rospy.ROSInterruptException: pass 
