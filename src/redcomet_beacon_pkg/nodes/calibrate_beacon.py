#! /usr/bin/env python

import roslib; roslib.load_manifest('mscs_beacon_code022013')
import rospy

from dynamixel_msgs.msg import MotorState
from std_msgs.msg import Float64
from std_msgs.msg import String

import math

class calibrate():

     def __init__(self):
          #Initialize new node
          rospy.init_node('rotate_beacon_node', anonymous=True)
          
          #Initialize Motor States
          self.motor1_state = MotorState()
          #self.motor2_state = MotorState()

          #dynamixel_test_namespace = rospy.get_namespace()
          dynamixel_test_namespace = 'dynamixel_controller/'
          rate = rospy.get_param('~rate', 1)
          r = rospy.Rate(rate)

		#Subscribe to motor status topic
          rospy.Subscriber('/dynamixel_controller/motor1_controller/state', MotorState, self.motor1_state)

		#Initialize publisher for servo1/top motor
          self.servo1_frame = 'servo1_link'
          self.servo1_pub = rospy.Publisher(dynamixel_test_namespace + 'motor1_controller/command', Float64)

		#Initialize publisher for servo2/bottom motor
          self.servo2_frame = 'servo2_link'
          self.servo2_pub = rospy.Publisher(dynamixel_test_namespace + 'motor2_controller/command', Float64)

		#Initialize servo1/2 positions to -90deg
          rospy.sleep(1)
          self.initialize_servos()
          rospy.loginfo("Laser Beacon Initialized")

				
     def initialize_servos(self):
          # Reset Servo 1 position to -90deg
          rospy.loginfo("Resetting Servo 1")	
          self.servo1_pub.publish(-1.57);
          # Reset Servo 2 position to -90deg
          rospy.loginfo("Resetting Servo 2")
          self.servo2_pub.publish(-1.57)
          rospy.sleep(10)
          

if __name__ == '__main__':
     try:
          calibrate()
          rospy.spin()
     except rospy.ROSInterruptException: pass 


